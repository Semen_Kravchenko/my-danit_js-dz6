//Теория
//foreach - это метод, который позволяет выполнить, прописанную в нём callback-функцию, по каждому элементу массива, к которому он применяется. В функцию прописываются параметры, первый из которых всегда будет обязательным, и будет применяться один раз для каждого элемента массива.

//Задание
function filterBy(newArr, dataType) {
    return newArr.filter(el => {
        return typeof el !== dataType;
    });
}
console.log( filterBy(['hello', 'world', 23, '23', null], "string") );
